package org.dataArt.multithreading;

/**
 * Ping Pong!
 */
public class PingPong extends Thread {
    private String last = "PONG";

    public static void main(String[] args) throws InterruptedException {
        PingPong pingPong = new PingPong();

        new Thread(() -> pingPong.action("ping")).start();

        new Thread(() -> pingPong.action("   PONG")).start();
        pingPong.join();
        pingPong.interrupt();
    }

    private synchronized void action(String message) {
        try {
            for (int i = 0; i <= 8; i++) {
                if (!isInterrupted()) {
                    if (last.equals(message)) {
                        try {
                            wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        System.out.println(message);
                        last = message;
                        try {
                            sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        notifyAll();
                    }

                } else {
                    throw new InterruptedException();
                }
            }
        } catch (InterruptedException e) {
            System.out.println("Thread is interrupted");
        }

    }
}
